# vanke_Star_Meeting
杭州万科星遇光年府相关资料
* [vanke_Star_Meeting](#vanke_star_meeting)
   * [一、星遇光年府位置](#一星遇光年府位置)
      * [本案图例](#本案图例)
      * [监管账户余额](#监管账户余额)
      * [本案户型图](#本案户型图)
         * [户型105](#户型105)
         * [户型129](#户型129)
         * [户型139](#户型139)
      * [本案日照](#本案日照)
      * [本案装修清单](#本案装修清单)
      * [本案地下车位分布图](#本案地下车位分布图)
      * [本案不利因素](#本案不利因素)
      * [相关规划](#相关规划)
   * [二、本案备案合同](#二本案备案合同)
   * [三、摇号流程参考](#三摇号流程参考)
   * [四、网签注意事项](#四网签注意事项)
   * [五、贷款相关](#四贷款相关)
      * [A、房贷](#a房贷)
      * [B、车位贷](#b车位贷)
   * [六、Q&amp;A](#五qa)
      * [1、连廊效果](#1连廊效果)
      * [2、交房缴纳费用](#2交房缴纳费用)
      * [3、车位销售情况](#3车位销售情况)
      * [4、前期物业管家联系方式](#4前期物业管家联系方式)
      * [5、勾庄剩余房源数量](#5勾庄剩余房源数量)
   * [七、项目进度](#六项目进度)
        * [2022-10-18](#2022-10-18)
        * [2022-09-25](#2022-09-25)
        * [2022-09-22](#2022-09-22)
        * [2022-09-08](#2022-09-08)
        * [2022-09-05](#2022-09-05)
        * [2022-08-30](#2022-08-30)

# **非常重要**
## 2022-09-30
最新更新，万科对置业指南部分条款做出解释，以下为情况说明
![WechatIMG48 -w300](assets/WechatIMG48.jpeg)

## 2022-09-30
关于**置业指南**不合理条款，回复说是国庆节会在售楼处公示，大家到时候可以去现场看看情况。
## 2022-09-29
关于**置业指南**中的**霸王条款**，已得到市场监管局的回复，市场监管部门约谈万科5人，所以请大家**团结一致**，共建美好家园！（以下是业主群内信息整理）:
1、接到市场监管局的电话了，今天万科一行5人（为首的是销售总朱总）到他们局里商谈我们投诉的置业指南霸王条款。经过沟通，万科说我们所签的置业指南条款会以购房合同为准，购房合同约定如有变更，10个工作日内告知业主，如果有业主有担心，可以联系销售，在置业指南上手写一句类似“以购房合同为准”的话，万科盖章，业主按手印。这个工作量其实蛮大的，市场监管局是建议他们出个公示，说明一下置业指南会以购房合同为准，但是万科说公示需要上级领导去批准，比较麻烦，先汇报上去没给确切的答复。

2、我和另外一个业主抵制签置业指南，提出在旁边备注不得侵害业主利益，被销售拒绝。当时我找了律师和销售对话，销售的态度也非常强势，说所有业主都签字了，要不老老实实签置业指南要不退房。我当时一个字都没签回家了，很憋屈。

3、12345回复说，签完霸王条款，维权就没有意义了

**希望大家能积极的采取合理的方式维权，做其他业主的后盾!**
杭州市房管局举报电话: 0571-89830105
余杭区房管局举报电话: 0571-89053039,0571-88566810
市场监管局电话 0571-88770955
电话 12345
#### **请关注业主公众号**
![qrcode_for_gh_9f9270992e68_344](assets/qrcode_for_gh_9f9270992e68_344.jpeg)



## 一、星遇光年府位置

![截屏2022-08-31 23.37.04](assets/%E6%88%AA%E5%B1%8F2022-08-31%2023.37.04.png)
[小区全景图](https://www.720yun.com/t/14vk6y2epqw?scene_id=101817406)

### 本案图例
![星遇光年府图例](assets/%E6%98%9F%E9%81%87%E5%85%89%E5%B9%B4%E5%BA%9C%E5%9B%BE%E4%BE%8B.jpeg)
楼层图例
![44641662607553_.pic](assets/44641662607553_.pic.jpg)

### 监管账户余额
 <div align="left">
    <img src="assets/WechatIMG21889.jpeg" width="300">
    <img src="assets/WechatIMG21888.jpeg" width="300">
 </div>


### 本案户型图
#### 户型105
视频: [105户型视频](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/roomplan/105video.mp4)
<div align="center">
    <img src="assets/IMG_8424.JPG" width="400">
</div>


#### 户型129
视频: [129户型视频](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/roomplan/129video.mp4)
<div align="center">
    <img src="assets/IMG_8423.JPG" width="400">
</div>

#### 户型139
视频: [139户型视频](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/roomplan/139video.mp4)
<div align="center">
    <img src="assets/IMG_8422.JPG" width="400">
</div>

### 本案日照
![640](assets/640.gif)

### 本案装修清单
![IMG_8399](assets/IMG_8399.JPG)
**美好家105的装修柜子尺寸**:
主卧：宽3.086 深0.6 高2.38 投影7.34
次卧：宽1.538 深0.6 高2.38 投影3.66
阳台：宽1.274 深0.68 高2.79 投影3.55
玄关+餐边柜+客厅：宽8.03 深0.35 高2.375 投影19.07


### 本案地下车位分布图
1、6、9、10地下一层分布![IMG_8425](assets/IMG_8425.JPG)
3、7地下一层分布
![IMG_9355](assets/IMG_9355.JPG)


### 本案不利因素
<div align="left">
    <img src="assets/IMG_8404.JPG" width="400">
    <img src="assets/IMG_8405.JPG" width="400">
</div>
<div align="left">
    <img src="assets/IMG_8406.JPG" width="400">
    <img src="assets/IMG_8407.JPG" width="400">
</div>



###     相关规划
1. 北部新城整体规划
    [北部新城规划文档](城北中央商务区规划.pdf)
   
   现状图 2020.08
   ![北部新城现状图2020.8](assets/%E5%8C%97%E9%83%A8%E6%96%B0%E5%9F%8E%E7%8E%B0%E7%8A%B6%E5%9B%BE2020.8.jpeg)
 规划图2020版本
 ![北部新城详细规划图](assets/%E5%8C%97%E9%83%A8%E6%96%B0%E5%9F%8E%E8%AF%A6%E7%BB%86%E8%A7%84%E5%88%92%E5%9B%BE.jpg)

2. 中心湖规划
    ![总平面](assets/%E6%80%BB%E5%B9%B3%E9%9D%A2.jpg)
## 二、本案备案合同

本次购房签署文档如下:

见文档[网签合同修改](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/contract/vanke20220907.pdf)-20220907修改版-修改了业主违约金。

<font color='red'> 置业指南</font>**（不合理条款主要在此合同）**: 见文档[置业指南](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/contract/zhiyezhinan.pdf)

临时管理规约:[临时管理规约](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/contract/linshiguanliguiyue.pdf)

前期物业服务合同: [前期物业服务合同](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/contract/qianqiwuyeguanlihetog.pdf)



见文档[网签合同原始](https://gitee.com/neobeyondme/vanke_Star_Meeting/blob/master/contract/vanke20220831.pdf)-20220831最初版。
    
## 三、摇号流程参考
参考知乎文章 [摇号之后流程](https://zhuanlan.zhihu.com/p/399411686?utm_medium=social&utm_oi=802356528442118144&utm_psn=1548270351741841408&utm_source=wechat_session&wechatShare=2&s_r=0)
    
## 四、网签注意事项    

1、封顶不要太快，正常干就行，否则留下主体隐患，墙体开裂之类。开发商为了封顶放款大概率会抢工。
2、关于减配。万科百分之99会减配，就看程度了，包括外立面、装修、配套等。合同里面约定的装标是3500的，有更专业伙伴过程中盯下万科，起码希望万科不要太难看。

3、关于监管资金。这个主要防止万科违规抽调资金 带来逾期及烂尾低品质风险，概率不大，但我们都承受不起，大家一起盯着吧。

4、项目四周一些重大规划落实情况，尤其东边学校，南侧工业园，是否会有新的不利因素出现，大家也留意哈。

5、最后一个就是物业这块，后面督促好万科吧，适当多一点点物业费可以接受，但品质一定要保证，一定！
签约时注意事项么 最大就是签每个字都看清楚 起码核心条款看下 一定不要急 毕竟几百万都付了 不差这个把小时。

6、按照销售通知的，带好材料，有专人带的。就是签字比较多 签字前仔细看好 尤其补充协议。签约时候不要急。

**<u>~~7、合同签完了，违约责任那个我直接在上面手写备注了 日息按万分之一，违约金按百分之1，对等！逾期付款和逾期交付一致。~~（此处网签合同的问题已在9月7日修改）</u>**

8、网签急撒，万科销售催，看看清楚，排后面网签也没事的———6-1801

**各位邻居，关于网签目前发现霸王条款一共有四处**

①置业指南，第8页，重要信息确认；

②置业指南，第37页，其它产品信息提示；

③商品房买卖合同，关于规划变更；

④商品房买卖合同，总的页数还不一样，大家可以注意是否有啥猫腻。

参考图片如下：
1、![WechatIMG41](assets/WechatIMG41.jpeg)
2、![WechatIMG42](assets/WechatIMG42.jpeg)
3、![WechatIMG43](assets/WechatIMG43.jpeg)




## 五、贷款相关
以下是群管理员汇总的本案贷款银行的表格:
### A、房贷
![截屏2022-08-31 23.54.48](assets/%E6%88%AA%E5%B1%8F2022-08-31%2023.54.48.png)
### B、车位贷

![截屏2022-11-14 15.33.23](assets/%E6%88%AA%E5%B1%8F2022-11-14%2015.33.23.png)



## 六、Q&A
### 1、连廊效果
目前连廊有效果图如下
<div align="center">
    <img src="assets/IMG_8421.JPG" width="400">
</div>

### 2、交房缴纳费用
常规的交房时候要缴纳的费用:
1.房子实测绘与预测绘的面积差额(多退少补)
2.物业维修基金(65元×实测绘面积)
3.物业费(4.15×实测绘面积)，应该是收一年的
4.车位管理费，不过买车位的送一年了

契税是办证的时候交的，办证交房后马上可以办，也可以等几年办，只不过看自己要不要卖，要卖房的早点办比较好(毕竟有满二唯一或者满五的免税政策)
ps:晚办产证的风险就是产权还属于开发商，万一开发商破产拿去抵押了
### 3、车位销售情况
红色是已经销售的，白色是剩余的
![103551662015071_.pic_hd](assets/103551662015071_.pic_hd.jpg)
### 4、前期物业管家联系方式
 <div align="left">
    <img src="assets/l86d28f97d137366d708809ff23251b1a-s534752fb426da545ef321b341bdb6900-mc29bb8e841105cf7dce3b05d62f377ef.jpg" width="400">
    <img src="assets/2.jpg" width="400">
 </div>



### 5、勾庄剩余房源数量
2022-09-25
1、星创城映月璟园 102-133㎡ 29000元/㎡ 1868套（余约100套未开）；
2、绿城·月映海棠园 95-160㎡ 31200元/㎡  1228套（余96套未开）；
3、绿城·春知海棠苑 110-160㎡ 31200元/㎡ 718套 （余398套未开）；
4、绿城·燕语海棠轩 104-139㎡ 31200元/㎡ 1116套 （余434套未开）；
5、万科星遇光年府 105-139㎡ 31200元/㎡ 882套 （余490套未开）★ 预计10月份加推
6、滨江福翠里  99-139㎡ 31200元/㎡ 664套 (余396套）；
7、滨江运翠轩 99-139㎡ 31200元/㎡ 840套 ；
8、地铁越秀星缦云渚  洋房 96-141㎡ 高层 98-118㎡  29090元/㎡  1794套 ★ 预计10月首开。
良渚项目：
1、万科星图光年轩 105-139㎡ 29100元/㎡ 1840套 （余950套）



## 七、项目进度
#### 2022-10-18
![WechatIMG60](assets/WechatIMG60.jpeg)

#### 2022-09-25
![WechatIMG20959](assets/WechatIMG20959.jpeg)
![WechatIMG20961](assets/WechatIMG20961.jpeg)
![WechatIMG20966](assets/WechatIMG20966.jpeg)
![WechatIMG20967](assets/WechatIMG20967.jpeg)
![WechatIMG20968](assets/WechatIMG20968.jpeg)

#### 2022-09-22
![WechatIMG18257](assets/WechatIMG18257.jpeg)
![WechatIMG18262](assets/WechatIMG18262.jpeg)
![WechatIMG18266](assets/WechatIMG18266.jpeg)
![WechatIMG18273](assets/WechatIMG18273.jpeg)
![WechatIMG18456](assets/WechatIMG18456.jpeg)
![WechatIMG18450](assets/WechatIMG18450.jpeg)
![WechatIMG18492](assets/WechatIMG18492.jpeg)
![WechatIMG18493](assets/WechatIMG18493.jpeg)



#### 2022-09-08
隔壁小区19楼视野
![WechatIMG5075](assets/WechatIMG5075.jpeg)

![WechatIMG5076](assets/WechatIMG5076.jpeg)
![WechatIMG5099](assets/WechatIMG5099.jpeg)
![WechatIMG5127](assets/WechatIMG5127.jpeg)


#### 2022-09-05
![WechatIMG1032](assets/WechatIMG1032.jpeg)


#### 2022-08-30
以下图: ![1](%E9%A1%B9%E7%9B%AE%E8%BF%9B%E5%BA%A6/2022-08-30/IMG_8408.JPG)
![IMG_8410](assets/IMG_8410.JPG)
![IMG_8412](assets/IMG_8412.JPG)





